﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ase_component_1;
using System.Drawing;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_validateInput()
        {
            string test = "Draw Circle a";
            string expectedResult = "Invalid Circle Radius in line 1\n";

            Validate val = new Validate();
            string actualResult = val.validateInput(test);
            Assert.AreEqual(expectedResult, actualResult);

        }

        
    }
}
