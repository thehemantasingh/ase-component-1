﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ase_component_1
{
    class Rectangle:Shape
    {
        int width, height;

        /// <summary>
        /// Constructor of Rectangle class
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }

        /// <summary>
        /// Parameterized Conatructor of Rectangle class
        /// </summary>
        /// <param name="colour">Color of Rectangle</param>
        /// <param name="x">Value on X Axis</param>
        /// <param name="y">Value on Y Axis</param>
        /// <param name="width">Width of rectangle</param>
        /// <param name="height">Height of Rectangle</param>
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width; //the only thingthat is different from shape
            this.height = height;
        }

        /// <summary>
        /// Methos to set color, plain value, and side values of Rectangle 
        /// </summary>
        /// <param name="colour">Color of Rectangle</param>
        /// <param name="list">An array of plain value and sides of Rectangle</param>
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        /// <summary>
        ///  Method to Draw Rectanfle
        /// </summary>
        /// <param name="g">Graphics</param>
        /// <param name="fillState">1 id to fill shape and 0 to unfill</param>
        public override void draw(Graphics g, int fillState)
        {
            if(fillState == 0)
            {
                Pen p = new Pen(colour, 2);
                g.DrawRectangle(p, x, y, width, height);
            }
            else
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillRectangle(b, x, y, width, height);
                g.DrawRectangle(p, x, y, width, height);
            } 
        }
    }
}
