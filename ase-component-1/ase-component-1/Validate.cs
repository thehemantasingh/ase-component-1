﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ase_component_1
{

    public class Validate
    {
        String userInput;
        Dictionary<string, int> variables = new Dictionary<string, int>();

        /// <summary>
        /// This method validats the User Input
        /// </summary>
        /// <param name="input">Value of TextBox</param>
        /// <returns>Retuens an error or null if no error exists</returns>
        public string validateInput(String input)
        {
            this.userInput = input.Trim();
            String[] lines = userInput.Split('\n');
            int linecount = 1;
            String error = null;

            foreach(String line in lines) { 
                String trimmedLine = line.Trim();
                String[] words = trimmedLine.Split(' ', ',');

                if (words[0].ToUpper() == "DRAW" && words.Length>1)
                {
                    switch (words[1].ToUpper())
                    {
                    case "CIRCLE":
                                if(words.Length == 3)
                                {
                                try
                                {
                                    Int32.Parse(words[2]);
                                }
                                catch (FormatException formatexcep)
                                {
                                    if (! variables.ContainsKey(words[2]))
                                    {
                                        error = error + "Invalid Circle Radius in line " + linecount + "\n";

                                    }

                                }
                            }
                                else
                                {
                                    error = error + "Invalid number of argument in line " + linecount + "\n";
                            }
                                break;
                    case "RECTANGLE":
                                if (words.Length == 4)
                                {
                                    try
                                    {
                                        Int32.Parse(words[2]);
                                    }
                                    catch (FormatException formatexcep)
                                    {
                                    if (!variables.ContainsKey(words[2]))
                                    {
                                        error = error + "Invalid Sides of Rectangle in line " + linecount + "\n";
                                    }
                                    }
                                    try
                                    {
                                        Int32.Parse(words[3]);
                                    }
                                    catch (FormatException formatexcep)
                                    {
                                    if (!variables.ContainsKey(words[3]))
                                    {
                                        error = error + "Invalid Sides of Rectangle in line " + linecount + "\n";
                                    }
                                    }

                            }
                                else
                                {
                                error = error + "Invalid number of argument in line " + linecount + "\n";
                            }
                                break;
                    case "TRIANGLE":
                                if (words.Length == 6)
                                {
                                    try
                                    {
                                        Int32.Parse(words[2]);
                                    }
                                    catch (FormatException formatexcep)
                                    {
                                        if (!variables.ContainsKey(words[2]))
                                        {
                                            error = error + "Invalid Sides of Triangle in line " + linecount + "\n";
                                        }
                                    }
                                try
                                {
                                    Int32.Parse(words[3]);
                                }
                                catch (FormatException formatexcep)
                                {
                                    if (!variables.ContainsKey(words[3]))
                                    {
                                        error = error + "Invalid Sides of Triangle in line " + linecount + "\n";
                                    }
                                }
                                try
                                {
                                    Int32.Parse(words[4]);
                                }
                                catch (FormatException formatexcep)
                                {
                                    if (!variables.ContainsKey(words[4]))
                                    {
                                        error = error + "Invalid Sides of Triangle in line " + linecount + "\n";
                                    }
                                }
                                try
                                {
                                    Int32.Parse(words[5]);
                                }
                                catch (FormatException formatexcep)
                                {
                                    if (!variables.ContainsKey(words[5]))
                                    {
                                        error = error + "Invalid Sides of Triangle in line " + linecount + "\n";
                                    }
                                }
                            }
                                else
                                {
                                     error = error + "Invalid number of argument in line " + linecount + "\n";
                            }
                                break;
                    default:
                            error = error + "Invalid shape in line " + linecount + "\n";
                            break;
                }

                } 
                else if(words[0].ToUpper() == "MOVETO")
                {
                    if(words.Length == 3)
                    {
                        try
                        {
                            Int32.Parse(words[1]);
                        }
                        catch (FormatException formatexcep)
                        {
                            if (!variables.ContainsKey(words[1]))
                            {
                                error = error + "Invalid Move Co-ordinates in line" + linecount + "\n";
                            }
                        }
                        try
                        {
                            Int32.Parse(words[2]);
                            //  Int32.Parse(words[2]);

                        }
                        catch (FormatException formatexcep)
                        {
                            if (!variables.ContainsKey(words[2]))
                            {
                                error = error + "Invalid Move Co-ordinates in line" + linecount + "\n";
                            }
                        }
                    }
                    else
                    {
                        error = error + "Invalid number of argument in line " + linecount + "\n";
                    }
                }
                else if (words[0].ToUpper() == "PEN")
                {
                    if(words.Length == 2)
                    {
                        if(!( words[1].ToUpper() == "BLUE" || words[1].ToUpper() == "BLACK" || words[1].ToUpper() == "RED" || words[1].ToUpper() == "GREEN"))
                        {
                            error = error + "Invalid color name in line " + linecount + "\n";
                        }
                    }
                    else
                    {
                        error = error + "Invalid number of argument in line " + linecount + "\n";
                    }
                }
                else if (words[0].ToUpper() == "FILL")
                {
                    if (words.Length == 2)
                    {
                        if (!(words[1].ToUpper() == "ON" || words[1].ToUpper() == "OFF"))
                        {
                            error = error + "Invalid fill argument in line " + linecount + "\n";
                        }

                    }
                    else
                    {
                        error = error + "Invalid number of argument in line " + linecount + "\n";
                    }
                }
                else if(words[0].ToUpper() == "WHILE")
                {
                    if(words.Length == 4)
                    {
                        Boolean checkEndLoop = false;
                        String[] lines1 = userInput.Split('\n');

                        foreach(String line1 in lines1)
                        {
                            String[] words1 = line1.Split(' ', ',');

                            if (words1[0].ToUpper() == "ENDLOOP")
                            {
                                checkEndLoop = true;
                            }
                        }


                        if (checkEndLoop == false)
                        {
                            error = error + "while loop not ended for line " + linecount + "\n";
                        }
                        else
                        {
                            if (variables.ContainsKey(words[1]))
                            {
                                if (words[2] == "==" || words[2] == ">" || words[2] == "<" || words[2] == ">=" || words[2] == "<=" || words[2] == "!=")
                                {
                                    try
                                    {
                                        Int32.Parse(words[3]);
                                    }
                                    catch (FormatException formatexcep)
                                    {
                                        if (!variables.ContainsKey(words[3]))
                                        {
                                            error = error + "Invalid number in line " + linecount + "\n";
                                        }
                                    }
                                    }
                                else
                                {
                                    error = error + "invalid operator in line " + linecount + "\n";

                                }
                            }
                            else
                            {
                                error = error + "undeclared variable in line " + linecount + "\n";
                            }
                        }
                    }
                    else
                    {
                        error = error + "Invalid number of argument in line " + linecount + "\n";
                    }
                }
                else if (words[0].ToUpper() == "ENDLOOP")
                {

                }
                else if(words[0].ToUpper() == "ENDIF"){

                }
                else
                {
                    if(words.Length > 1 && words[1] == "=") { 
                        if (words.Length == 3)
                        {
                            try
                            {

                                if (variables.ContainsKey(words[0]))
                                {
                                    variables[words[0]] = Int32.Parse(words[2]);
                                }
                                else
                                {
                                    variables.Add(words[0], Int32.Parse(words[2]));
                                }
                            }
                            catch (FormatException formatexcep)
                            {
                                error = error + "Invalid variable value in line" + linecount + "\n";
                            }
                        }
                        else if(words.Length == 5)
                        {
                            if(words[2] == words[0])
                            {
                                if(words[3] == "+" || words[3] == "-" || words[3] == "/" || words[3] == "*" )
                                {
                                    try
                                    {
                                        Int32.Parse(words[4]);
                                    }
                                    catch (FormatException formatexcep)
                                    {
                                        error = error + "Invalid value in line " + linecount + "\n";
                                    }
                                }
                                else
                                {
                                    error = error + "Invalid operator in line " + linecount + "\n";
                                }
                            }
                            else
                            {
                                error = error + "Invalid variable name in line " + linecount + "\n";
                            }
                        }
                        else
                        {
                            error = error + "Invalid number of argument in line " + linecount + "\n";
                        }
                    }
                    else
                    {
                        error = error + "Invalid Command in line " + linecount + "\n";
                    }
                }
                linecount++;
            }
            return error;
        }
    }
}
