﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ase_component_1
{/// <summary>
/// A class that implements Shapes intergface 
/// </summary>
    abstract class Shape : Shapes
    {
        protected Color colour; 
        protected int x, y; 

        /// <summary>
        /// Constructor of Shape Class
        /// </summary>
        public Shape()
        {
            colour = Color.Red;
            x = y = 100;
        }

        /// <summary>
        /// Parameterized Constructor of Shape Class
        /// </summary>
        /// <param name="colour">Color od Shape</param>
        /// <param name="x">Value on X axix</param>
        /// <param name="y">Value on Y axis</param>
        public Shape(Color colour, int x, int y)
        {
            this.colour = colour; 
            this.x = x; 
            this.y = y; 
        }


        public abstract void draw(Graphics g, int fillState);

        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
        }
    }
}
