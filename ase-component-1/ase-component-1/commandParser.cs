﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ase_component_1
{

    public class commandParser
    {
        Draw myDraw = (Draw)Application.OpenForms[0];


        Graphics g;
        int xAxis, yAxis, fillState;
        Color color;
        int startPoint,endPoint , lineCount;
        Dictionary<string, int> variables;
        Dictionary<string, string> methods;
        PictureBox drawArea;
        RichTextBox textArea_userInput;
        TextBox textBox_actionInput;

        /// <summary>
        /// A method to Read Individual line
        /// </summary>
        public void commandParse()
        {
            variables = new Dictionary<string, int>();
            methods = new Dictionary<string, string>();


            textArea_userInput = myDraw.getUserInput();
            textBox_actionInput = myDraw.getActionInput();
            drawArea = myDraw.getDrawArea();
            g = drawArea.CreateGraphics();
            startPoint = 0;

            String actionValue = textBox_actionInput.Text;
            String userInput = textArea_userInput.Text;

            if (actionValue.ToUpper() == "RUN")
            {
                String error = null;


            //    Validate p = new Validate();
            //    error = p.validateInput(userInput);

                if (error == null)
                {
                    String[] lines = userInput.Trim().Split('\n');
                    endPoint = lines.Length;

                    parseWord(userInput, startPoint, endPoint);
                    
                }
                else
                {
                    MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if (actionValue.ToUpper() == "CLEAR")
            {
                drawArea.Refresh();
            }
            else if (actionValue.ToUpper() == "RESET")
            {
                xAxis = 0;
                yAxis = 0;
                textArea_userInput.Text = "";
                drawArea.Refresh();
                color = Color.Black;
            }
            else
            {
                MessageBox.Show("Invalid Action", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// A method to read individual word of a line
        /// </summary>
        /// <param name="userInput">Input from user</param>
        /// <param name="startPoint">Value to start line parsing</param>
        /// <param name="endPoint">Value to stop line parsing</param>
        public void parseWord(String userInput, int startPoint, int endPoint)
        {
            Color color = Color.Black;
            fillState = 0;
            /*xAxis = 0;
            yAxis = 0;*/
            drawArea = myDraw.getDrawArea();
            g = drawArea.CreateGraphics();

            String[] lines = userInput.Trim().Split('\n');
            for (lineCount = startPoint; lineCount < endPoint; lineCount++)
            {

                String[] words = lines[lineCount].Trim().Split(' ', ',');

                if (words[0].ToUpper() == "DRAW")
                {
                    if (words[1].ToUpper() == "CIRCLE")
                    {
                        ShapeFactory sf = new ShapeFactory();
                        Shape sa = sf.getShape("CIRCLE");

                        int radiusValue;
                        try
                        {
                            radiusValue = Int32.Parse(words[2]);
                        }
                        catch (FormatException formatexcep)
                        {
                            radiusValue = variables[words[2]];
                        }
                        sa.set(color, xAxis, yAxis, radiusValue);
                        sa.draw(g, fillState);


                       // textArea_userInput.Text = color + "\n" + xAxis + "\n" + yAxis + "\n" + variables[variableKoValueIndex];
                    }
                    else if (words[1].ToUpper() == "RECTANGLE")
                    {
                        ShapeFactory sf = new ShapeFactory();
                        Shape sa = sf.getShape(words[1]);
                        int side1, side2;
                        try
                        {
                            side1 = Int32.Parse(words[2]);

                        }
                        catch (FormatException formatexcep)
                        {
                            side1 = variables[words[2]];
                        }
                        try
                        {
                            side2 = Int32.Parse(words[3]);

                        }
                        catch (FormatException formatexcep)
                        {
                            side2 = variables[words[3]];
                        }
                        sa.set(color, xAxis, yAxis, side1, side2);
                        sa.draw(g, fillState);
                    }
                    else if (words[1].ToUpper() == "TRIANGLE")
                    {
                        ShapeFactory sf = new ShapeFactory();
                        Shape sa = sf.getShape(words[1]);
                        int side1, side2, side3, side4;
                        try
                        {
                            side1 = Int32.Parse(words[2]);
                        }
                        catch (FormatException formatexcep)
                        {
                            side1 = variables[words[2]];
                        }
                        try
                        {
                            side2 = Int32.Parse(words[3]);
                        }
                        catch (FormatException formatexcep)
                        {
                            side2 = variables[words[3]];
                        }
                        try
                        {
                            side3 = Int32.Parse(words[4]);

                        }
                        catch (FormatException formatexcep)
                        {
                            side3 = variables[words[4]];
                        }
                        try
                        {
                            side4 = Int32.Parse(words[5]);
                        }
                        catch (FormatException formatexcep)
                        {
                            side4 = variables[words[5]];
                        }

                        sa.set(color, xAxis, yAxis, side1, side2, side3, side4);
                        sa.draw(g, fillState);
                    }
                }

                else if (words[0].ToUpper() == "MOVETO")
                {
                    
                    try
                    {
                        xAxis = Int32.Parse(words[1]);

                    }
                    catch (FormatException formatexcep)
                    {
                        xAxis = variables[words[1]];
                    }
                    try
                    {
                        yAxis = Int32.Parse(words[2]);

                    }
                    catch (FormatException formatexcep)
                    {
                        yAxis = variables[words[2]];
                    }
                }
                else if (words[0].ToUpper() == "PEN")
                {
                    color = Color.FromName(words[1]);
                }
                else if (words[0].ToUpper() == "FILL")
                {
                    if (words[1].ToUpper() == "ON")
                    {
                        fillState = 1;
                    }
                    else
                    {
                        fillState = 0;
                    }
                }
                else if (words[0].ToUpper() == "WHILE")
                {
                    int startLine = lineCount + 1;
                    int testline = startLine;
                    String[] words1 = lines[lineCount].Trim().Split(' ', ',');

                    while (words1[0].ToUpper() != "ENDLOOP")
                    {
                        words1 = lines[testline].Trim().Split(' ', ',');
                        testline++;
                    }

                    int endLine = testline-1 ;

                   // MessageBox.Show("Start " + startLine + "\nENd " + endLine);



                    int test = variables[words[1]];
                    int test1 = Int32.Parse(words[3]);

                    if (words[2] == "==")
                    {
                        while (test == Int32.Parse(words[3]))
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];
                        }
                    }
                    if (words[2] == ">")
                    {
                        while (test > test1)
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];

                        }
                    }
                    if (words[2] == "<")
                    {
                        while (test < Int32.Parse(words[3]))
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];
                        }
                    }
                    if (words[2] == ">=")
                    {
                        while (test >= Int32.Parse(words[3]))
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];
                        }
                    }
                    if (words[2] == "<=")
                    {
                        while (test <= Int32.Parse(words[3]))
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];
                        }
                    }
                    if (words[2] == "!=")
                    {
                        while (test != Int32.Parse(words[3]))
                        {
                            parseWord(userInput, startLine, endLine);
                            test = variables[words[1]];
                        }
                    }

                }
                else if(words[0].ToUpper() == "IF")
                {   
                    if (words.Contains( "THEN" ))
                    {
                        String temp = string.Join(" ", words);
                        String temp1 = string.Join(" ", words[0],words[1],words[2],words[3],words[4]);
                        String ifStatement = temp.Replace(temp1, "");

                        if (words[2] == "==")
                        {
                            if(variables[words[1]] == Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                        if (words[2] == ">")
                        {
                            if (variables[words[1]] > Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                        if (words[2] == "<")
                        {
                            if (variables[words[1]] < Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                        if (words[2] == ">=")
                        {
                            if (variables[words[1]] >= Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                        if (words[2] == "<=")
                        {
                            if (variables[words[1]] <= Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                        if (words[2] == "!=")
                        {
                            if (variables[words[1]] != Int32.Parse(words[3]))
                            {
                                parseWord(ifStatement, 0, 1);
                            }
                        }
                    }
                    else
                    {
                        int startLine = lineCount + 1;
                        int testline = startLine;
                        String[] words1 = lines[lineCount].Trim().Split(' ', ',');

                        while (words1[0].ToUpper() != "ENDIF")
                        {
                            words1 = lines[testline].Trim().Split(' ', ',');
                            testline++;
                        }

                        int endLine = testline;

                        if (words[2] == "==")
                        {
                            if (variables[words[1]] == Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine,endLine);
                            }
                        }
                        if (words[2] == ">")
                        {
                            if (variables[words[1]] > Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine, endLine);
                            }
                        }
                        if (words[2] == "<")
                        {
                            if (variables[words[1]] < Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine, endLine);
                            }
                        }
                        if (words[2] == ">=")
                        {
                            if (variables[words[1]] >= Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine, endLine);
                            }
                        }
                        if (words[2] == "<=")
                        {
                            if (variables[words[1]] <= Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine, endLine);
                            }
                        }
                        if (words[2] == "!=")
                        {
                            if (variables[words[1]] != Int32.Parse(words[3]))
                            {
                                parseWord(userInput, startLine, endLine);
                            }
                        }

                    }
                }
                else if (words[0].ToUpper() == "ENDLOOP")
                {

                }
                else if(words[0].ToUpper() == "ENDIF"){

                }
                else if (words[0].ToUpper() == "ENDMETHOD")
                {

                }
                else if(words[0].ToUpper() == "METHOD"){

                    int startLine = lineCount + 1;
                    String[] words1 = lines[lineCount].Trim().Split(' ', ',');

                    while (words1[0].ToUpper() != "ENDMETHOD")
                    {
                        words1 = lines[lineCount].Trim().Split(' ', ',');
                        lineCount ++;
                    }
                    int endLine = lineCount - 1;
                    String methodTask = null;

                    for(int i = startLine; i<endLine; i++)
                    {
                        methodTask = methodTask + lines[i] + "\n";
                    }

                    methods.Add(words[1], methodTask);

                    MessageBox.Show(words[1] + "\n" + methodTask);
                }
                
                else
                {
                    if (methods.ContainsKey(words[0]))
                    {
                        String[] methodLines = methods[words[0]].Trim().Split('\n');
                        parseWord(methods[words[0]], 0, methodLines.Length);
                    }
                    if (words.Length == 3)
                    {
                        if (variables.ContainsKey(words[0]))
                        {
                            variables[words[0]] = Int32.Parse(words[2]);
                        }
                        else
                        {
                            variables.Add(words[0], Int32.Parse(words[2]));
                        }
                        
                    }
                    else
                    {
                        if(words[3] == "+")
                        {
                            variables[words[0]] = variables[words[0]] + Int32.Parse(words[4]);
                        }
                        if (words[3] == "-")
                        {
                            variables[words[0]] = variables[words[0]] - Int32.Parse(words[4]);
                        }
                        if (words[3] == "/")
                        {
                            variables[words[0]] = variables[words[0]] / Int32.Parse(words[4]);
                        }
                        if (words[3] == "*")
                        {
                            variables[words[0]] = variables[words[0]] * Int32.Parse(words[4]);
                        }
                    }

                }
            }

        }


        /// <summary>
        /// Method to save program in txt file
        /// </summary>
        public void saveProgram()
        {
            RichTextBox textArea_userInput = myDraw.getUserInput();

            StreamWriter fWriter = File.CreateText("D:\\assignment.txt");
            fWriter.Write(textArea_userInput.Text);
            fWriter.Close();

            MessageBox.Show("File Saved");
        }

        /// <summary>
        /// Method to load program from txt file
        /// </summary>
        public void loadProgram()
        {
            RichTextBox textArea_userInput = myDraw.getUserInput();

            try
            {
                StreamReader s = File.OpenText("D:\\assignment.txt");
                do
                {
                    string line = s.ReadLine();
                    if (line == null) break;
                    textArea_userInput.Text += line + "\n";

                } while (true);
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("ERROR", "Cannot find hemanta.txt");
            }
            catch (IOException ie)
            {
                MessageBox.Show("Error", "IO Exception ");
            }
        }
    }
}
