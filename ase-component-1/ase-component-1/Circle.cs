﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ase_component_1
{
     class Circle:Shape
    {
        int radius;

        /// <summary>
        /// Constructor of Circle Class
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// Parameterized Constructor of Circle Class
        /// </summary>
        /// <param name="colour">Color of Circle</param>
        /// <param name="x">Value on X Axis</param>
        /// <param name="y">Value on Y Axis</param>
        /// <param name="radius">Radius of Circle</param>
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {
            this.radius = radius; 
        }


        /// <summary>
        /// Method to set color, plain value and radius of Circle
        /// </summary>
        /// <param name="colour">Color of Circle</param>
        /// <param name="list">A array of Plane Value and radius of circle</param>
        public override void set(Color colour, params int[] list)
        {
 
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
        }



        public override void draw(Graphics g, int fillState)
        {

            if(fillState == 0){
                Pen p = new Pen(colour, 2);
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
            else
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillEllipse(b, x, y, radius * 2, radius * 2);
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
            
            
            

        }
    }
}
