﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ase_component_1
{
    class Triangle : Shape
    {

        int firstSide, secondSide, thirdSide, fourthSide;
        /// <summary>
        /// This is constructor of Triangle class
        /// </summary>
        public Triangle() : base()
        {

        }
        /// <summary>
        /// This is Parameterized constructor of Triangle class 
        /// </summary>
        /// <param name="colour">Color of triangle</param>
        /// <param name="positionX">Position on X axis</param>
        /// <param name="positionY">Position on Y axis</param>
        /// <param name="firstSide">value of 1st point of triangle</param>
        /// <param name="secondSide">value of 2nd point of triangle</param>
        /// <param name="thirdSide">value of 3rd point of triangle</param>
        /// <param name="fourthSide">value of 4th point of triangle</param>
        public Triangle(Color colour, int positionX, int positionY, int firstSide, int secondSide, int thirdSide, int fourthSide) : base(colour, positionX, positionY)
        {
            this.firstSide = firstSide;
            this.secondSide = secondSide;
            this.thirdSide = thirdSide;
            this.fourthSide = fourthSide;
        }
        /// <summary>
        /// Method to set points, plain value and color of Triangle
        /// </summary>
        /// <param name="colour">Color of Triangle</param>
        /// <param name="list">Array of plain value and Points of Triangle</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.firstSide = list[2];
            this.secondSide = list[3];
            this.thirdSide = list[4];
            this.fourthSide = list[5];
        }
        /// <summary>
        /// Method to draw triangle
        /// </summary>
        /// <param name="g">Graphics</param>
        /// <param name="fillState">1 to fill shape and 0 to unfill </param>
        public override void draw(Graphics g, int fillState)
        {
            Point[] trianglePoints = { new Point(x, y), new Point(firstSide, secondSide), new Point(thirdSide, fourthSide) };
            

            if (fillState == 1)
            {
                Pen p = new Pen(colour, 2);
                SolidBrush b = new SolidBrush(colour);
                g.FillPolygon(b, trianglePoints);
                g.DrawPolygon(p, trianglePoints);


            }
            else
            {
                Pen p = new Pen(colour, 2);
                g.DrawPolygon(p, trianglePoints);
            }

        }
    }
}
