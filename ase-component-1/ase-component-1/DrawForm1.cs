﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ase_component_1
{
    public partial class Draw : Form
    {

        

        /// <summary>
        /// This is constructor of Draw class
        /// </summary>
        public Draw()
        {
            InitializeComponent();
        }

        public PictureBox getDrawArea()
        {
            return drawArea;
        }

        public RichTextBox getUserInput()
        {
            return textArea_userInput;
        }

        public TextBox getActionInput()
        {
            return textBox_actionInput;
        }

        /// <summary>
        /// ActionListener for "Go button"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_go_Click(object sender, EventArgs e)
        {
            commandParser cop = new commandParser();
            cop.commandParse();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commandParser myCommandParser = new commandParser();
            myCommandParser.saveProgram();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commandParser myCommandParser = new commandParser();
            myCommandParser.loadProgram();
        }
    }
}